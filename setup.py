from package import Package
from setuptools import setup, find_packages


with open('VERSION', 'r') as f:
    version = f.read()


setup(
    name='color_utils',
    version=version,
    maintainer='Omri Treidel',
    maintainer_email='omri.treidel@dragontailsystems.com',
    description='color utilities',
    packages=find_packages(),
    package_data={'': ['VERSION']},
    include_package_data=True,
    install_requires=[
        'numpy>=1.14',
        'PyYAML',
        'pytest',
        'pytest-codecheckers',
    ],
    test_suite='nose.collector',
    tests_require=['nose'],
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Operating System :: POSIX :: Linux'
    ],
    cmdclass={
        "package": Package
    }
)
