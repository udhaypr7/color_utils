"""
helper methods for computing the features
"""

import numpy as np

from skimage import measure
from skimage.morphology import convex_hull_image
from skimage.transform import resize

from color_utils.pizza_colors import PizzaClasses


pizza_classes = PizzaClasses.default().filter('name', 'Unknown')
toppings = pizza_classes.get_toppings()
non_topping_labels = set(pizza_classes.labels) - set(toppings.labels)


class Top2ProbabilityError(Exception):
    pass


def get_pizza_area(labels):
    return float((labels > 0).sum())


def _compute_top_2_probabilities_diff(probabilities):
    sorted_prob = np.sort(probabilities, axis=-1)
    try:
        return sorted_prob[..., -1] - sorted_prob[..., -2]
    except IndexError:
        msg = "probabilities don't have 2 elements in the last dimention"
        raise Top2ProbabilityError(msg)


def relative_c_hull_area(mask, pizza_area):
    """ Area of the convex hull of a mask divided by the area of the pizza

    Args:
        mask (np.array): binary mask
        pizza_area (float): area of the pizza

    Returns:
        float: relative area
    """
    h, w = mask.shape[0:2]
    new_h = (h // 2)
    new_w = (w // 2)
    area_ratio = h * w / (new_h * new_w)
    small_mask = resize(mask, (new_h, new_w), mode='reflect')
    try:
        c_hull_area = convex_hull_image(small_mask).sum()
    except ValueError:
        c_hull_area = 0.0
    return round(c_hull_area * area_ratio / pizza_area, 5)


def compute_features(class_probabilities, labels):
    """

    Args:
        class_probabilities (np.array): H x W x number_of_classes
        labels (np.array): H x W: class labels

    Returns:
        np.array: features
    """

    num_features = 7
    pizza_area = get_pizza_area(labels)
    top_2_prob_diff = _compute_top_2_probabilities_diff(class_probabilities)

    features = np.zeros((len(pizza_classes.labels), num_features))
    unique_labels = np.unique(labels)
    for label in (set(unique_labels) & set(toppings.labels)):
        mask = labels == label
        # mean and median are different when the distribution is skewed
        _mean_prob = class_probabilities[mask, label].sum()
        _med_prob = np.median(class_probabilities[mask, label])
        _mean_prob_diff = top_2_prob_diff[mask].mean()
        _prob_sum = class_probabilities[..., label].sum() / pizza_area

        _c_hull_area = relative_c_hull_area(mask, pizza_area)

        region_labels = measure.label(mask)
        region_areas = np.array([region.area / pizza_area for region in
                                 measure.regionprops(region_labels)])

        features[label, :] = [_mean_prob, _med_prob, _mean_prob_diff,
                              _prob_sum, _c_hull_area,
                              len(region_areas), region_areas.mean()]

    return features
