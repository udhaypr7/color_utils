import numpy as np

from color_utils import training_tools as ttools


def test_to_one_hot():
    _1_hot = ttools.to_one_hot(np.array([1, 3, 4]), num_classes=6)
    assert (_1_hot == np.array([0, 1, 0, 1, 1, 0])).all()
