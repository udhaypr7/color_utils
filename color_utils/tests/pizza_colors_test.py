from unittest import TestCase

import numpy as np

from color_utils import get_toppings_rgb
from color_utils.pizza_colors import PizzaClasses, InvalidLabelError, UNKNOWN


def test_PizzaClasses():
    pizza_classes = PizzaClasses.default()
    expected_num_classes = 31
    assert pizza_classes.num_classes == expected_num_classes
    assert pizza_classes.names[-1] == UNKNOWN


def test_PizzaClasses_sort():
    pizza_classes = PizzaClasses.default()
    expected_last_name = 'Vegan Cheese'
    assert pizza_classes.sort('name').names[-1] == expected_last_name


def test_PizzaClasses_select():
    """Test full cycle of selecting by name, get the corresponding labels
    and select by the labels.

    """
    pizza_classes = PizzaClasses.default()
    _expected = ['Onion', 'Pepperoni']
    items = pizza_classes.select('name', ['Onion', 'Pepperoni'])
    assert set(items.select('label', items.labels).names) == set(_expected)
    assert set(items.select('rgb', items.rgbs).names) == set(_expected)


def test_PizzaClasses_filter():
    pizza_classes = PizzaClasses.default()
    toppings = pizza_classes.filter('category', ['other']).all
    assert set([item.category for item in toppings]) == set(['toppings'])


def test_counting():
    pizza_classes = PizzaClasses.default()
    expected_num_toppings = 24
    assert pizza_classes.num_toppings == expected_num_toppings

    expected_num_other = 7
    assert pizza_classes.num_others == expected_num_other


class TestImport(TestCase):
    def setUp(self):
        # prepare an image with all RGB values
        self.pizza_classes = PizzaClasses.default()
        self.num_classes = self.pizza_classes.num_classes
        size = int(np.ceil(np.sqrt(self.num_classes)))
        rgb_labels = np.zeros((size, size, 3), 'uint8')
        self.image = np.reshape(rgb_labels, (size ** 2, 3))
        for i, color in enumerate(self.pizza_classes.rgbs):
            self.image[i, :] = list(color)
        self.rgb_labels = np.reshape(self.image, (size, size, 3))

    def test_get_toppings_rgb(self):
        """Testing the structure of the RGB values YML and the exact number
        of fields """
        rgb_values = get_toppings_rgb()
        # check the structure
        expected_keys_lvl_0 = set(['other', 'toppings'])
        self.assertEquals(set(rgb_values.keys()), expected_keys_lvl_0,
                          'Parsed proper fields from the YAML file')
        expected_keys_lvl_1 = set(['name', 'rgb'])
        self.assertEquals(set(rgb_values['other'][0].keys()),
                          expected_keys_lvl_1,
                          'Parsed proper fields from the YAML file')
        # test number of fields
        expected_num_other = 7
        self.assertEquals(len(rgb_values['other']), expected_num_other)
        expected_num_toppings = 24
        self.assertEquals(len(rgb_values['toppings']), expected_num_toppings,
                          'Parsed number of toppings from the YAML file')
        # test all names start with uppercase and RGB have 3 values
        for item in (rgb_values['other'] + rgb_values['toppings']):
            self.assertTrue(item['name'][0].isupper())
            self.assertEquals(len(item['rgb']), 3)

    def test_rgb_to_labels(self):
        """Test the conversion of RGB colors to labels is correct"""
        labels = self.pizza_classes.to_labels(self.rgb_labels, True)
        flat_labels = labels.flatten()
        # test that unknown RGB are converted to -1
        invalid_inds = list(np.where(flat_labels == -1)[0])
        expected_invalid_inds = range(self.num_classes, self.image.shape[0])
        self.assertEquals(invalid_inds, expected_invalid_inds)

        # generate the labels manually
        expected_labels = []
        for i, color in enumerate(self.pizza_classes.rgbs):
            label = self.pizza_classes.select('rgb', [color]).labels[0]
            expected_labels.append(label)

        self.assertEquals(list(flat_labels)[:self.num_classes],
                          expected_labels)

    def test_labels_to_rgb(self):
        """Test full cycle RGB -> labels -> RGB"""
        labels = self.pizza_classes.to_labels(self.rgb_labels, True)
        self.assertRaises(InvalidLabelError, self.pizza_classes.to_rgb, labels)

        valid_labels = self.pizza_classes.to_labels(self.rgb_labels, True)
        invalid_selection = valid_labels == -1
        new_label = 0
        new_label_rgb = self.pizza_classes.select('label', [new_label]).rgbs[0]
        valid_labels[invalid_selection] = new_label
        self.rgb_labels[invalid_selection, :] = list(new_label_rgb)
        rgb = self.pizza_classes.to_rgb(valid_labels)
        self.assertTrue((rgb == self.rgb_labels).all())
