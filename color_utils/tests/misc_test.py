import numpy as np

from datetime import datetime, timedelta
from skimage.morphology import binary_dilation, square

from color_utils import misc
from color_utils.pizza_colors import PizzaClasses

PIZZA_CLASSES = PizzaClasses.default()


def get_labels():
    params = dict()
    params['width'] = 40
    params['height'] = 40
    labels = np.zeros((params['height'], params['width']))

    cheese_label = PIZZA_CLASSES.select('name', 'Regular Cheese').labels[0]
    params['cheese_width'] = 20
    params['cheese_height'] = 20
    labels[10:30, 10:30] = cheese_label

    mask = labels == cheese_label
    params['crust_thickness'] = 2
    square_size = params['crust_thickness'] + 1
    crust_sel = binary_dilation(mask, square(square_size)) & ~mask
    labels[crust_sel] = PIZZA_CLASSES.select('name', 'Crust').labels[0]

    params['onion_width'] = 5
    params['onion_height'] = 5
    onion_x = slice(15, 15 + params['onion_width'])
    onion_y = slice(15, 15 + params['onion_height'])
    labels[onion_y, onion_x] = PIZZA_CLASSES.select('name', 'Onion').labels[0]

    params['beef_width'] = 4
    params['beef_height'] = 4
    beef_x = slice(21, 21 + params['beef_width'])
    beef_y = slice(21, 21 + params['beef_height'])
    params['beef_x'] = beef_x
    params['beef_y'] = beef_y
    labels[beef_y, beef_x] = PIZZA_CLASSES.select('name', 'Beef').labels[0]
    return labels, params


def test_pizza_area():
    labels, params = get_labels()
    crust_width = params['crust_thickness']
    height = params['cheese_height'] + crust_width
    width = params['cheese_width'] + crust_width
    expected_area = height * width
    assert misc.pizza_area(labels) == expected_area


def test_topping_area():
    labels, params = get_labels()
    beef_area = misc.topping_area(labels, 'Beef', relative=False)
    expected_area = params['beef_height'] * params['beef_width']
    assert beef_area == expected_area


def test_topping_mask():
    labels, params = get_labels()
    mask_beef = misc.topping_mask(labels, 'Beef')
    mask_expected = np.zeros((params['height'], params['width']), bool)
    mask_expected[params['beef_y'], params['beef_x']] = True
    assert (mask_expected == mask_beef).all()


def test_parse_iso_datetime():
    """test iso parsing"""
    now_utc = '2018-07-09T23:49:16.515476+00:00'
    now_bne = now_utc[:-6] + '+10:00'
    now_ny = now_utc[:-6] + '-08:00'

    expected_datetime = datetime(2018, 7, 9, 23, 49, 16, 515476)
    expected_offset = timedelta(0)
    _datetime, _offset = misc.parse_iso_timestamp(now_utc)
    assert expected_datetime == _datetime
    assert expected_offset == _offset

    expected_offset = timedelta(hours=10)
    _datetime, _offset = misc.parse_iso_timestamp(now_bne)
    assert expected_datetime == _datetime
    assert expected_offset == _offset

    expected_offset = timedelta(hours=-8)
    _datetime, _offset = misc.parse_iso_timestamp(now_ny)
    assert expected_datetime == _datetime
    assert expected_offset == _offset


def test_version():
    from color_utils import version
    assert isinstance(version, str)


def test_scale():
    np.random.seed(42)
    data = np.random.randn(10, 3)
    mean = np.random.randn(10, 3).mean(axis=0)
    std = np.random.randn(10, 3).std(axis=0)
    assert np.max(
        np.abs((data - mean)/std - misc.scale(data, mean, std))) < 1e-6

    data = np.random.randn(10, 3, 4)
    mean = np.random.randn(10, 3, 4).mean(axis=0)
    std = np.random.randn(10, 3, 4).std(axis=0)
    assert np.max(
        np.abs((data - mean)/std - misc.scale(data, mean, std))) < 1e-6


def test_calculate_center_mass():
    mask = np.zeros((5, 5), int)
    assert (misc.calculate_center_mass(mask) == np.array([2, 2])).all()

    mask[1, 1] = 1
    expected_center = np.array([1, 1])
    assert (misc.calculate_center_mass(mask) == expected_center).all()

    mask[1, 3] = 1
    expected_center = np.array([1, 2])
    assert (misc.calculate_center_mass(mask) == expected_center).all()


def test_calculate_mask_center():
    mask_even = np.zeros((4, 4))
    res_1 = misc.calculate_mask_center(mask_even)
    assert (res_1 == np.array([2, 2])).all()

    mask_odd = np.zeros((5, 5))
    res_2 = misc.calculate_mask_center(mask_odd)
    assert (res_2 == np.array([2, 2])).all()
