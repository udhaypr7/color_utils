import numpy as np

from color_utils import misc, polar
from color_utils.pizza_colors import PizzaClasses
from color_utils.doughtype_features import DoughTypes
from color_utils.doughtype_features import mad, _raw_features, compute_features


PIZZA_CLASSES = PizzaClasses.default()


def get_labels():
    params = dict()
    params['width'] = 400
    params['height'] = 400
    labels = np.zeros((params['height'], params['width']))

    Z = polar.complex_grid(labels)
    r = np.abs(Z)

    cheese_label = PIZZA_CLASSES.select('name', 'Regular Cheese').labels[0]
    crust_label = PIZZA_CLASSES.select('name', 'Crust').labels[0]
    R = 100.0
    dR = 2.0
    labels[r < R] = cheese_label
    labels[(r >= R) & (r < (R + dR))] = crust_label
    params['R'] = R
    params['dR'] = dR
    return labels, params


def test_types():
    assert DoughTypes(0).name == 'classic'


def test_mad():
    np.random.seed(42)
    array = np.random.laplace(size=100)
    med = np.median(array)
    expected = np.median(np.abs(array - med))
    assert mad(array) == expected


def test_features():
    labels, params = get_labels()
    center = misc.calculate_center_mass(labels > 0)
    crust_mask = misc.topping_mask(labels, 'Crust')
    num_sectors = 16
    area = float((~misc.topping_mask(labels, 'Background')).sum())
    r, dr = _raw_features(crust_mask, center, num_sectors)
    expected_r = (params['R'] + 0.5 * params['dR'])
    assert abs(np.median(r) - expected_r) < 0.5  # half a pixel tolerance
    expected_dr = params['dR'] / 4
    assert abs(np.median(dr) - expected_dr) < 1e-2

    scaled_r, scaled_dr = compute_features(crust_mask, center, num_sectors,
                                           area)
    assert (scaled_r == r/np.sqrt(area)).all()
    assert (scaled_dr == dr / r).all()
