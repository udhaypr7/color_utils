import numpy as np

from color_utils import misc


PI = np.pi


def complex_grid(image, center=None):
    """ generate a complex grid around the image center

    Args:
        image (np.array): an image
        center (iterable): center of the grid Y, X. If non will be the center
            of the image

    Returns:
        np.array: complex grid
    """
    height, width = image.shape[:2]
    if center is None:
        center = misc.calculate_mask_center(image)
    y = np.arange(height) - center[0]
    x = np.arange(width) - center[1]
    X, Y = np.meshgrid(x, y)
    return X + 1j*Y


def divide_image(image, num_sectors, center=None, offset=0.0, eps=1e-12):
    """ Divide image to sectors

    Args:
        image (np.array): a image
        num_sectors (int): number of sector
        center (iterable): origin of the frame of reference
        offset (float): offset angle
        eps (float): ensuring that the angles is in [0, 2pi)

    Returns:
        np.array: labels of the sector
    """
    grid = complex_grid(image, center=center) * np.exp(1j * offset)
    # make sure all angles are in the range [0, 2PI)
    angles = np.angle(grid) + PI - eps
    sector_angle = (2*PI/num_sectors)
    sector_labels = (angles / sector_angle).astype(int)
    # save 0 label for background
    return sector_labels + 1, grid
