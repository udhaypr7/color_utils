import numpy as np

from collections import namedtuple

from color_utils import get_toppings_rgb

UNKNOWN = 'Unknown'

PizzaClass = namedtuple('PizzaClass', ['name', 'label', 'rgb', 'category'])


class InvalidLabelError(Exception):
    pass


class PizzaClasses(object):

    def __init__(self, classes):
        self._classes = classes
        self._hash_table = None

    @classmethod
    def default(cls):
        _rgb_values = get_toppings_rgb()
        _classes = []
        count = 0
        for category in _rgb_values.keys():
            for color in _rgb_values[category]:
                if color['name'] == UNKNOWN:
                    unknown_color = tuple(color['rgb'])
                    unknown_category = category
                    continue
                _classes.append(PizzaClass(name=color['name'],
                                           label=count,
                                           rgb=tuple(color['rgb']),
                                           category=category))
                count += 1

        # add unknown at the end of the list
        _classes.append(PizzaClass(name=UNKNOWN,
                                   label=count,
                                   rgb=unknown_color,
                                   category=unknown_category))
        return cls(_classes)

    @property
    def all(self):
        return self._classes

    def sort(self, by):
        return PizzaClasses(sorted(self.all, key=lambda x: getattr(x, by)))

    def select(self, attribute, values):
        return PizzaClasses(
            filter(lambda x: getattr(x, attribute) in values, self.all))

    def filter(self, attribute, values):
        return PizzaClasses(
            filter(lambda x: getattr(x, attribute) not in values, self.all))

    def get_toppings(self):
        return self.select('category', 'toppings')

    def get_others(self):
        return self.select('category', 'other')

    @property
    def num_classes(self):
        return len(self.all)

    @property
    def num_toppings(self):
        return self.get_toppings().num_classes

    @property
    def num_others(self):
        return self.get_others().num_classes

    def _get_values(self, attribute):
        return map(lambda x: getattr(x, attribute), self.all)

    @property
    def labels(self):
        return self._get_values('label')

    @property
    def names(self):
        return self._get_values('name')

    @property
    def rgbs(self):
        return self._get_values('rgb')

    def _hash_rgb(self, rgb):
        return np.dot(np.array(rgb), self._hash_coeffs) % self._prime

    def _set_hash_coeffs(self):
        np.random.seed(42)
        self._prime = 1051
        while True:
            self._hash_coeffs = np.random.choice(self._prime, 2, replace=False)
            self._hash_coeffs = np.append(self._hash_coeffs, 1)
            hashed_rgbs = [self._hash_rgb(rgb) for rgb in self.rgbs]
            if len(set(hashed_rgbs)) == len(self.rgbs):
                break

    def generate_hash_table(self):
        # invalid values have label -1
        self._set_hash_coeffs()
        self._hash_table = np.zeros(self._prime, np.int8) - 1
        for _class in self.all:
            hashed_rgb = self._hash_rgb(_class.rgb)
            self._hash_table[hashed_rgb] = _class.label

    def to_labels(self, image, as_labels=False):
        """ Convert hard annotation to integer labels

        Args:
            image (np.array): height x width x channels image
            as_labels (bool): if true return as labels otherwise as one hot

        Returns:
            np.array: labels or one hot encoding
        """
        if self._hash_table is None:
            self.generate_hash_table()

        image = image[..., :3]
        labels = self._hash_table[self._hash_rgb(image)]
        if as_labels:
            return labels
        one_hot = (labels[..., np.newaxis] == np.arange(self.num_classes))
        return one_hot.astype(np.int8)

    def to_rgb(self, labels):
        """Given sparsely encoded labels in array of shape (height, width, 1)
        or (height, width) return array of shape (height, width, 3) as per RGB
        codes used by the annotators
        """
        image = np.zeros(labels.shape[:2] + (3,), dtype='uint8')
        unique_labels = np.unique(labels)
        if -1 in unique_labels:
            raise InvalidLabelError("Cannot invert -1 label")

        for label in unique_labels:
            ind_x, ind_y = np.where(labels == label)
            _rgb = self.select('label', [label]).rgbs[0]
            image[ind_x, ind_y, :] = np.array(_rgb, dtype='uint8')
        return image
