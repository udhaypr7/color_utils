import os

import h5py
import numpy as np
import time
import uuid

from keras import backend as K

from color_utils import version
from color_utils.misc import get_timestamp


def add_metadata(model_filename, **kwargs):
    """ Add metadata to Keras model file """
    with h5py.File(model_filename, 'r+') as f:
        if 'metadata' not in f:
            f.create_group('metadata')

        for key, value in kwargs.items():
            if isinstance(value, str) or isinstance(value, unicode):
                f['metadata/' + key] = np.string_(value)
            else:
                f['metadata/' + key] = value


def add_normalization_params(model_filename, mean, std):
    """ Append mean and std of the training data to the model file

    Args:
        model_filename (str): path to HDF5 model file
        mean (np.array): mean of the training data
        std (np.array): std of the training data
    """
    with h5py.File(model_filename, 'r+') as f:
        grp = f.create_group('normalization_params')
        grp['mean'] = mean
        grp['std'] = std


def load_normalization_params(model_path):
    with h5py.File(model_path, 'r') as f:
        mean = f['normalization_params/mean'][()]
        std = f['normalization_params/std'][()]
    return mean, std


def save_model_for_prod(full_model_file_path, get_empty_model, mean, std,
                        prod_model_filename=None, **kwargs):
    """ Save model without metrics/loss and with mean and std

    Args:
        full_model_file_path (str): Path tp Keras model with metrics/loss
        get_empty_model (function): function to get an empty model
        mean (np.array): mean of the training data
        std (np.array): std of the training data
        prod_model_filename (str): Path to save striped model
    """
    K.set_learning_phase(0)
    prod_model = get_empty_model()
    prod_model.load_weights(full_model_file_path)

    if prod_model_filename is None:
        name, ext = os.path.splitext(full_model_file_path)
        prod_model_filename = name + '-prod' + ext
    print("Saving model to %s", prod_model_filename)
    prod_model.save(prod_model_filename)

    add_normalization_params(prod_model_filename, mean, std)
    utc_time = get_timestamp()[1]
    add_metadata(prod_model_filename, timestamp=int(time.time()),
                 utc_time=utc_time, uuid=str(uuid.uuid4()),
                 color_utils_version=version, **kwargs)


def _read_h5_metadata(metadata_grp):
    metadata = dict()
    for k in metadata_grp.keys():
        metadata[k] = metadata_grp[k][()]
    return metadata


def read_features(data_path):
    """ Read metadata, features and labels from HDF5 file"""
    with h5py.File(data_path, 'r') as f:
        metadata = _read_h5_metadata(f['metadata'])
        features = np.array(f['features'])
        labels = np.array(f['labels'])
    return metadata, features, labels
