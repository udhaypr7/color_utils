import re

import datetime
import numpy as np
import pytz
import tzlocal


from color_utils.pizza_colors import PizzaClasses


def get_timestamp():
    """
    Returns:
        str: current timestamp in local timezone
        str: current timestamp in UTC
    """
    local_now = datetime.datetime.now(tzlocal.get_localzone())
    utc_now = datetime.datetime.now(pytz.utc)
    return (datetime.datetime.isoformat(local_now),
            datetime.datetime.isoformat(utc_now))


def parse_iso_timestamp(timestamp):
    """

    Args:
        timestamp (str): iso timestamp string YYYY-MM-DDTHH:mm:ss.ssssss+HH:mm

    Returns:
        datetime.datetime: date time object
        datetime.timedelta: time delta from UTC
    """
    date, time_with_shift = timestamp.split('T')
    time, offset = re.split(r'[+,-]', time_with_shift)
    sign = re.findall(r'[+,-]', time_with_shift)[0]
    elements = map(int, date.split('-'))
    elements += map(int, re.split(r'[:,.]', time))
    hours, minutes = map(int, offset.split(':'))
    offset_from_utc = datetime.timedelta(hours=hours, minutes=minutes)
    offset_from_utc = offset_from_utc if sign == '+' else -offset_from_utc
    return datetime.datetime(*elements), offset_from_utc


def scale(data, mean, std, eps=1e-10):
    return (data - mean)/(std + eps)


def semi_transparent(image, mask, alpha_val=0.6):
    """Generate a semi transparent image

    Args:
        image (np.array): 3 or 4 channel RGB/RGBA image
        mask (np.array): foreground mask
        alpha_val (float): used for the background

    Returns:
        np.array: RGBA image
    """
    mask = mask.astype('uint8')
    alpha = mask + (1 - mask) * alpha_val
    alpha *= 255
    alpha = alpha.astype('uint8')
    new_image = image.copy()
    if new_image.shape[-1] == 4:
        new_image[..., -1] = alpha
    elif new_image.shape[-1] == 3:
        new_image = np.concatenate([new_image, alpha[..., np.newaxis]],
                                   axis=-1)
    return new_image


def calculate_mask_center(image):
    return np.array(image.shape[:2]) // 2


def calculate_center_mass(mask):
    """find the center of mass of the mask. If the mask is all 0 return
    the center of the mask

    Args:
        mask (np.array): 0 for background

    Returns:
        (np.array) of the [row, col], i.e. Y, X
    """

    if mask.any():
        row_sector, col_sector = np.where(mask > 0)
        return np.array([int(row_sector.mean()),
                         int(col_sector.mean())])
    return calculate_mask_center(mask)


def topping_mask(labels, topping_name):
    """Creates a mask of give topping

    Args:
        labels (np.array): image size array with class label for every pixel
        topping_name (str): name of a topping

    Returns:
        np.array: binary mask for the topping
    """
    pizza_classes = PizzaClasses.default()
    topping_label = pizza_classes.select('name', topping_name).labels[0]
    return labels == topping_label


# FIXME: we should probably stop using because it's quite slow
def topping_area(labels, topping_name, relative=True):
    """

    Args:
        labels (np.array): image size array with class label for every pixel
        topping_name (str): name of a topping
        relative (bool): if True returns area relative to pizza area

    Returns:
        absolute or relative area of a topping
    """
    _topping_area = float(topping_mask(labels, topping_name).sum())
    if relative:
        return _topping_area/max(pizza_area(labels), 1.0)  # avoid 0 division
    return _topping_area


def pizza_area(labels, pizza_classes=PizzaClasses.default()):
    bg_label = pizza_classes.select('name', 'Background').labels[0]
    return float((labels != bg_label).sum())
