import os
import fnmatch

import h5py
import numpy as np
import random

from skimage.transform import resize

from color_utils import misc


def segment_image(image, mean, std, model, divisibility_factor=64):
    H, W = image.shape[0:2]
    nH = (H // divisibility_factor) * divisibility_factor
    nW = (W // divisibility_factor) * divisibility_factor
    image = resize(image, (nH, nW), preserve_range=True)
    image = misc.scale(image, mean, std)
    class_probability = model.predict(image[np.newaxis, ...])
    class_probability = class_probability.squeeze(axis=0)
    labels = np.argmax(class_probability, axis=-1)
    return class_probability, labels


def find_files(directory, pattern):
    """ Walk through the directory tree and find alll files that match the
    pattern

    Args:
        directory (str): directory at the top of the tree
        pattern (str): pattern for match i.e. '*.jpg'

    Returns:
        list[str]: list of files path
    """
    file_names = []
    for root, dirs, files in os.walk(directory):
        for basename in files:
            if fnmatch.fnmatch(basename, pattern):
                filename = os.path.join(root, basename)
                file_names.append(filename)
    return file_names


def split_files_list(files_list, num_val):
    if num_val >= len(files_list):
        raise ValueError("num_val must be smaller that total numer of files")
    val_files = random.sample(files_list, num_val)
    train_files = list(set(files_list) - set(val_files))
    return train_files, val_files


def to_one_hot(inds, num_classes):
    """Convert an array of integers to one hot (extension of numpy.where)

    Args:
        inds (np.array): array of labels/indices
        num_classes (int): number of classes

    Returns:
        np.array: array of 1s and 0s representing the same labels

    Example:
        [1,3,4], num_classes=6 -> [0 ,1, 0, 1, 1, 0]
    """
    labels = np.zeros(num_classes, int)
    if inds.size > 0:
        labels[inds] = 1
    return labels


def save_features(out_path, image_names, labels, features, segmenter_metadata):
    max_length = max([len(x) for x in image_names]) + 2

    with h5py.File(out_path, 'w') as f:
        f.create_dataset("image_names", (len(image_names),),
                         dtype="S%s" % max_length,
                         data=np.array(image_names))
        f['features'] = np.array(features)
        f['labels'] = np.array(labels)
        for key, value in segmenter_metadata.items():
            f['metadata/segmenter_' + key] = segmenter_metadata[key]
